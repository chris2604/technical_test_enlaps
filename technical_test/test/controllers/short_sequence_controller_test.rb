require 'test_helper'

class ShortSequenceControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get short_sequence_index_url
    assert_response :success
  end

end
