require "rails_helper"

RSpec.describe ShortSequence, type, :model do

	it 'should persist a short sequence' do

		count = ShortSequence.count

		ShortSequence.create(name: "Saluuuut")

		expect(ShortSequence.count).to eq(count +1)

	end

end