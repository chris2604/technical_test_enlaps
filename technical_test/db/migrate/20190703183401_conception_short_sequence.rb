class ConceptionShortSequence < ActiveRecord::Migration[5.2]
  def change
  	create_table :ShortSequence do |t|
  		t.integer :tikee_id, null: false
  		t.string :name, null: false
  		t.text :description
  		t.timestamp :start, null: false
  		t.integer :interval, null: false, default: 10
  		t.integer :duration, null: false, default: 86400
  		t.boolean :upload_to_cloud, null: false, default: true
  		t.string :image_format, null: false, default: ('jpeg')
  		t.boolean :keep_local_copy, null: false, default: false
  		t.integer :sequence_id, :limit => 8, null: false
  		t.string :shooting_status
  		t.integer :nb_images_on_sd, null: false, default: 0
  		t.integer :nb_images_uploaded, null: false, default: 0
  	end
  end
end
