# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_03_183409) do

  create_table "LongSequence", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", force: :cascade do |t|
    t.integer "tikee_id", null: false
    t.string "name", null: false
    t.text "description"
    t.timestamp "start", null: false
    t.timestamp "end", null: false
    t.boolean "upload_to_cloud", default: true, null: false
    t.string "image_format", default: "jpeg", null: false
    t.boolean "keep_local_copy", default: false, null: false
    t.boolean "infinite_duration", default: false, null: false
    t.bigint "sequence_id", null: false
    t.string "shooting_status"
    t.integer "nb_images_on_sd", default: 0, null: false
    t.integer "nb_images_uploaded", default: 0, null: false
  end

  create_table "ShortSequence", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", force: :cascade do |t|
    t.integer "tikee_id", null: false
    t.string "name", null: false
    t.text "description"
    t.timestamp "start", null: false
    t.integer "interval", default: 10, null: false
    t.integer "duration", default: 86400, null: false
    t.boolean "upload_to_cloud", default: true, null: false
    t.string "image_format", default: "jpeg", null: false
    t.boolean "keep_local_copy", default: false, null: false
    t.bigint "sequence_id", null: false
    t.string "shooting_status"
    t.integer "nb_images_on_sd", default: 0, null: false
    t.integer "nb_images_uploaded", default: 0, null: false
  end

end
