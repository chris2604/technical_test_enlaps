class LongSequence < ApplicationRecord
	self.table_name = "LongSequence"

	validates :nb_images_on_sd, :numericality =>
	{ 
		:greater_than_or_equal_to => 0,
		message: "Your nb_images_on_sd number should be greater than zero or equal"
	}
	validates :nb_images_uploaded, :numericality =>
	{ 
		:greater_than_or_equal_to => 0,
		message: "Your nb_images_uploaded number should be greater than zero or equal"
	}
end