class ShortSequence < ApplicationRecord
	self.table_name = "ShortSequence"

	def return_end_time(start_time, duration_time)
		date_to_sec = convert_date_to_timestamp(start_time)
		duration_to_sec = convert_min_to_sec(duration_time)
		return date_to_sec + duration_to_sec
	end

	def convert_date_to_timestamp(date)
		return date.to_i
	end

	def convert_min_to_sec(duration_time)
		return duration_time.minutes.to_i
	end

	#get all the sequences that will not allow our current sequence
	def get_array_of_conflict(array)
		puts "Trying to get all the sequences"
		puts "Then calculate the end point of each sequence"
		puts "If the Start Time of my sequence is bigger than the Start Time + Duration Time of each other Sequences"
		puts "And if the Start Time + the Duration Time of my sequence is smaller than the Start Time of every other Sequences"
		puts "So, the current sequence doesn't overlap and we can add it to the Database."
		return "end"
	end

	def are_there_any_conflict(array)
		if array.size>0
			return true
		end
		return false
	end

	validates :interval, :numericality =>
	{ 
		:greater_than_or_equal_to => 0,
		message: "Your interval number should be greater than zero or equal"
	}
	validates :duration, :numericality =>
	{ 
		:greater_than_or_equal_to => 0,
		message: "Your duration number should be greater than zero or equal"
	}
	validates :nb_images_on_sd, :numericality =>
	{ 
		:greater_than_or_equal_to => 0,
		message: "Your nb_images_on_sd number should be greater than zero or equal"
	}
	validates :nb_images_uploaded, :numericality =>
	{ 
		:greater_than_or_equal_to => 0,
		message: "Your nb_images_uploaded number should be greater than zero or equal"
	}

end