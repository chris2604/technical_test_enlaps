class ShortSequencesController < ApplicationController

	def index
		@shortSequence = ShortSequence.all
		@shortSequenceJson = @shortSequence.to_json()
	end

	def show
		@shortSequence = ShortSequence.find(params[:id])
	end

	def edit
		@shortSequence = ShortSequence.find(params[:id])
	end

	def update
		@shortSequence = ShortSequence.find(params[:id])

		if @shortSequence.update(get_post_params)
		    redirect_to short_sequences_path
		else
			render :show
		end
	end

	def new
		@shortSequence = ShortSequence.new
	end

	def create
		##
		#array_short_sequences =  ShortSequence.all

		#ength_conflict = get_array_of_conflict(array_short_sequences)
		#overlaps = are_there_any_conflict(length_conflict)

		#if overlaps
		#    redirect_to short_sequences_path
		#else
		#	@shortSequence = ShortSequence.create(get_post_params)
		#end
		@shortSequence = ShortSequence.create(get_post_params)
		redirect_to short_sequences_path
 
	end

	def destroy
		@shortSequence = ShortSequence.find(params[:id])

		@shortSequence.destroy
		
		redirect_to short_sequences_path
 
	end

	private

	def get_post_params
		params.require(:short_sequence).permit(:tikee_id, :name, :description, :start, :interval, :duration, :upload_to_cloud, :image_format, :keep_local_copy, :sequence_id, :shooting_status, :nb_images_on_sd, :nb_images_uploaded)
	end

end