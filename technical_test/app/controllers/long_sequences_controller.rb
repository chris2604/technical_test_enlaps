class LongSequencesController < ApplicationController
	def index
		@longSequence = LongSequence.all
	end

	def show
		@longSequence = LongSequence.find(params[:id])
	end

	def edit
		@longSequence = LongSequence.find(params[:id])
	end

	def update
		@longSequence = LongSequence.find(params[:id])
		
		if @longSequence.update(post_params)
			redirect_to long_sequences_path

		else
			render :show
		end
	end

	def new
		@longSequence = LongSequence.new
	end

	def create
		@longSequence = LongSequence.create(post_params)
		redirect_to long_sequences_path
 
	end

	def destroy
		@longSequence = LongSequence.find(params[:id])

		@longSequence.destroy
		
		redirect_to long_sequences_path
	end

	private

	def post_params
		params.require(:long_sequence).permit(:tikee_id, :name, :description, :start, :end, :upload_to_cloud, :image_format, :keep_local_copy, :infinite_duration, :sequence_id, :shooting_status, :nb_images_on_sd, :nb_images_uploaded)
	end

end